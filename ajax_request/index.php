
<!DOCTYPE html>
<html lang="de">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>AIO Blacklisting</title>

        <link href="styles/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="styles/css/style.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet"> 
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

        <script type="text/javascript" src="scripts/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="styles/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script language="JavaScript" type="text/javascript" src="scripts/cio.js"></script>
    </head>

    <body>

        <div class="section-clearspace"></div>

          <!-- OPTIN -->
        <section class="optin" id="form" style="background-color:transparent;">
            <div class="container">
                <div class="row">
                    <div class=" col-md-12">
                        <h2 class="align-middle" style="text-align: center; color: #7abe73">AIO Blacklisting</h2>
                    </div>
                    <div class="col-lg-12" style="padding-top: 8px;">
                        <!-- FORM BEGIN -->
                        <form style="background-color:#fff; padding:25px; border-radius:15px;" id="api-data-form" enctype="multipart/form-data"   method="post" accept-charset="utf-8" class="form-horizontal check-submit-form" role="form">

                            <div class="form-row">
                                <div class="form-group col-md-6">    
                                    <!-- INPUT APIKey -->
                                    <input style="color:#000;background:transparent;" value="" class="btn-outline-dark input-fields form-control" id="" name="api_key" placeholder="*MIO APIKey" type="text" required">          
                                    <!-- ./INPUT APIKey -->
                                </div>
                                <div class="form-group col-md-6">
                                    <!-- INPUT Blacklist Id -->
                                    <input style="color:#000; ;background:transparent;" value="" class="btn-outline-dark input-fields form-control" id="nachname" name="blk_id" placeholder="*Blacklist ID Number" type="text" required">
                                    <!-- ./INPUT Blacklist Id --> 
                                </div>
                            </div>

                            <!-- ./ INPUT ANREDE FRAU & HERR -->

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <!-- INPUT Email Address -->
                                    <input style="color:#000;background:transparent;" value="" class="btn-outline-dark input-fields form-control" id="" name="blk_email" placeholder="*Email address saperated by ','(comma)" type="text" required>           
                                    <!-- ./INPUT Email Address -->
                                </div>
                                <div class="form-group col-md-6">
                                    <!-- INPUT HAUS NO -->
                                    <input style="color:#000; ;background:transparent;" value="" class="btn-outline-dark input-fields form-control" id="" name="blk_imp_name" placeholder="*filter Name" type="text">
                                    <!-- ./INPUT NACHNAME -->
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">     
                                    <input style="color: #fff; cursor: pointer; background-color: #7abe73; border-color: #7abe73;" type="submit" class="col-sm-12 btn btn-xl js-scroll-trigger" value="Submit" name="submit" style="width: 100%;">   
                                </div>
                            </div>
                            <!-- ./ INPUT ANREDE FRAU & HERR -->

                            <div class="alert alert-success error_success" role="alert"><i class="fas fa-exclamation-circle"></i>&nbsp;</div>


                        </form>
                        <!-- ./ FROM END -->
                    </div>          
                </div>  
            </div>
        </section>
        <!-- ./OPTIN -->

        <div class="section-clearspace"></div>

        <!-- Footer -->
<!--        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 h-100 text-center text-lg-left my-auto">
                        <ul class="list-inline mb-2">
                            <li class="list-inline-item">
                                <a href="https://treaction.net/rechtliches/#datenschutz" target="_blank">Datenschutz</a>
                            </li>
                            <li style="color:#fff;" class="list-inline-item">&sdot;</li>
                            <li class="list-inline-item">
                                <a href="https://treaction.net/rechtliches/#impressum" target="_blank">Impressum</a>
                            </li>
                        </ul>
                        <p style="color:#c5c5c5!important;" class="text-muted small mb-4">&copy; treaction ag 2019</p>
                    </div>
                    <div class="col-lg-6 h-100 text-center text-lg-right my-auto">
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item mr-3">
                                <a href="#">
                                   <a href="https://treaction.net" target="_blank"><img style="width:250px;" src="img/tre-logo-weiss_retina.png" /></a>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>-->
        </div>

        <!-- Bootstrap core JavaScript -->
        <script src="scripts/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="scripts/js.device.detector-master/dist/jquery.device.detector.js"></script>
    </body>
</html>
