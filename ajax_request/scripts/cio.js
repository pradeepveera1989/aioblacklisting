/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$ ( document ).ready ( function () {

    $ ( '.error_success' ).hide ();

    /* JQuery submit event for the postal code
     * Parameters : 
     * Returns : 
     *    1. check the postal code and accordingly display the error message for postal code.
     */


    function logResults(json) {
        console.log ( json );
    }

    form = $ ( '.check-submit-form' );
    form.submit ( function (e) {

        var form = $ ( this );
        $ ( "input[type=submit]" ).hide ();
        let formData = form.serializeArray ();

        let formJson = JSON.stringify ( formData );
        let formbtoa = btoa ( formJson );
        let aio_url = "https://dev-api-in-one.net/blacklist?callback=";
        $.ajax ( {
            url: aio_url,
            type: 'GET',
            headers: {
                'Access-Control-Allow-Origin': 'https://dev-api-in-one.net/blacklist',
                'Access-Control-Allow-Methods': 'PUT, GET, POST, DELETE',
                'Access-Control-Allow-Headers': 'access-control-allow-headers,access-control-allow-methods,access-control-allow-origin,access-control-max-age,content-type,bearer',
                'Access-Control-Max-Age': '86400'
            },
            crossDomain: true,
            jsonpCallback: 'logResults',
            dataType: 'jsonp',
            contentType: 'application/x-www-form-urlencoded',
            data: {data: formbtoa},
            success: function (data) {
                var resp = $.parseJSON ( data );
                if ( resp === "success" ) {
                    $ ( '.error_aio' ).hide ();
                    $ ( '.error_success' ).empty ().append ( 'Ihr Kontakt wurde erfolgreich übermittelt' ).show ();
                } else {
                    $ ( "input[type=submit]" ).show ();
                    $ ( '.error_aio' ).empty ().append ( eval ( data ) ).show ();
                }
            },

            error: function (xhr, status, errorMessage) {
                console.log ( 'xhr', xhr );
                console.log ( 'textstatus', status );
                console.log ( 'error', errorMessage );
            },


        } );

        function logResults(json) {
            console.log ( json );
        }

        return false;
    } );
} );
