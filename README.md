**AIO BLacklisting Service **

Blacklisting service can be integrated using Ajax / Curl request.

Requirements
   -   MIO api-key
   -   MIO Blacklisting number
   -   MIO Blacklisting email address
   -   MIO Blacklisting filter name 

**CURL Based Blacklisting Service**
-   Curl based converts all the requied parameters to an array and does a POST curl request to 
URL : https://dev-api-in-one.net/blacklist.

    

**Ajax Based Blacklisting Service**
-   This service utlilzes ajax jsonp request to send data to URL : https://dev-api-in-one.net/blacklist.