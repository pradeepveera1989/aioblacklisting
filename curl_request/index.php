<?php

// data in array
$data = [
    // Mail-in-one account api-key
    ['name' => 'api_key', 'value' => '55d89848-415b-4842-9fda-e3b1d6cee9ea'],
    // account blacklist id.
    ['name' => 'blk_id', 'value' => '39435'],
    // Email address to be blacklisted, saparated by comma ','
    ['name' => 'blk_email', 'value' => 'spam_3@spam.com'],
    // blacklist filter name.
    ['name' => 'blk_imp_name', 'value' => 'test'],
];


// Convert array to json string.
$json_data = json_encode($data);
// Convert Json string to base64 string.
$curl_postfields = 'data=' . base64_encode($json_data);
$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => "https://dev-api-in-one.net/blacklist",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => $curl_postfields,
    CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "content-type: application/x-www-form-urlencoded",
        "cache-control: no-cache",
    ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
    echo "cURL Error #:" . $err;
} else {
    echo $response;
}

